#! /bin/bash

# Folder path
WATCH_DIR="~/scripts/"

# Function for permissions
set_execute_permission() {
     chmod +x "$1"
     chmod 600 "$1"
}

# Folder monitoring
inotifywait -m -e create -e moved_to --format "%w%f" "$WATCH_DIR" | while read FILE
do
    if [ -f "$FILE" ]; then
        set_execute_permission "$FILE"
        echo "Установлены права на выполнение для файла: $FILE"
    fi
done
