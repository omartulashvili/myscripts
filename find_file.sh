#!/bin/bash

FILENAME=$1

# Ищем файл во всей файловой системе
FOUND_FILE=$(find / -type f -name "$FILENAME" 2>/dev/null)

if [ -n "$FOUND_FILE" ];

then  
  echo "Файл $FILENAME найден в: $FOUND_FILE"
  # Отрываем новую вкладку терминала с директорией, где найден файл
  gnome-terminal --working-directory="$(dirname "$FOUND_FILE")"

else
  echo "Файл $FILENAME не найден"
fi
